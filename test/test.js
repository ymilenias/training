require('mocha')

const chai = require("chai");
const chaiHttp = require("chai-http");

chai.use(chaiHttp);

const {assert, expect, should, request} = chai;

describe("reqres api", function () {
    describe("endpoint", function (){
      
        it('response status should be http 200, when success (async await', async function() {
            const res = await request("https://reqres.in/api").get("/unknown");
            expect(res.status).to.be.equal(200);
            });

        it('response status should be http 200, when success (promise)', function() {
            request("https://reqres.in/api")
            .get("/unknown")
            .end((err, res) => {
                expect(res.status).to.be.equal(200);
            });
        
        
            //const array = [];

           // assert.equal(array.length, 0);
            // expect(array.length).to.be.an('array');
            // array.length.should.to.be.equal(0);
        })
    });

});

